Modname: Ledginstric

Mod Id: Led.

Version: "x.x"

Author: Narial

Ad: 
https://www.youtube.com/channel/UCjgfMsSGYXv78U0yhzTbAZw
https://sites.google.com/ulnight.net/ledginstric/home

Theme: Voodo Steampunk

Catagory: Adventure, Industry, Expanded Teirs

Tribute: "I would like to thank the development team and Killabi, who without their help and creation of Noble Fates this mod would not be possible."

Potential Conflicts:
	Terrain index: "8" "9" "10" "11"

Mod Coverage:

- Terrain
	- Bopha
	- Ceall
	- Proxite
	- Ignus

- Shelves
	- Stone
	- Mela

- Weapons
	- Gon
	- Gunts
	- Martar

- Learning
	- Menial Reference reading and studying

- Crafing Tables
	- Lecturns to read and study at
- Furnaces
	- Indus Core
	- Eleo Core

- Machines
	- Menial Assembler
	- Maximal Assembler
	- Caster
	- Blast Furnace
	- BlastCaster

- Items
	- Trinkets
	- Mela Ingot
	- Meel Ingot
	- Ores
	- Menial References

- Walls+other
	- Mela
		- Walls
		- Roofs
		- Doors
		- Floors
		- Angled Walls
		- Upside down Roofs
	- Stone door Arches
	- Scaffolding
		- Mela
		- Wood
	- Brick Walls

- C# Intergration
	- No social gatherings at modded furnaces.
	- Eleo Core does not require chimneys underground
